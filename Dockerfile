#Dockerfile
FROM centos:latest
MAINTAINER luzlinares@2008@gmail.com
RUN yum -y install httpd
COPY build/es6-unbundled /var/www/html/
CMD ["/usr/sbin/httpd", "-D", "FOREGROUND"]
EXPOSE 80
